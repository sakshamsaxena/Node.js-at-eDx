# Node.js Course Assignments

## Module 1 Assignment Lab: CSV to JSON Converter

#### Steps to run

```bash
cd Module-1
npm install
npm run main
```

#### Design

The Script was intended to be as flexible and robust as it can be. Scenarios when the CSV is very large to load in the memory, or when it doesn't have any CRLF/LF at the end of file, were considered as well.

To overcome large file reading issue, Streams were used instead of `readFile` to read and process chunks of data and discarding the same after that. Extraction operations are lightweight so not much data is buffered up to a heap as well. Low chunk length situations are covered as well.

Thus, only the core `fs` module was used, and rest was written in ES6.

#### Testing

```bash
cd Module-1
npm test
```

## Module 2 Assignment Lab: RESTful Blog API

#### Steps to run

```bash
cd Module-2
npm install
npm run main
```

#### Design

Used the `mergeParams` property of Express Router to actually abstract away the Comment Routes for a good design.

#### Testing

```bash
cd Module-2
npm test
```

## Module 3 Assignment Lab: MongoDB Migration Node Script

#### Steps to run

```bash
cd Module-3
npm install
node index.js BATCHSIZE
```

#### Design

Uses custom error handling for CLI Arguments. Abstracted away the DB insertions to a different file.

## Module 4 Assignment Lab: REST API with Mongoose

#### Steps to run

```bash
cd Module-4
npm install
npm run main
```

#### Design

Used a different file for Routes. Also opened and closed connection to db each time a request was made, to avoid unnecessary open connections.