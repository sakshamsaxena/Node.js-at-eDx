#!/bin/bash
echo "GET /posts"
curl http://localhost:3000/posts

echo -e "\nPOST /posts"
curl http://localhost:3000/posts -H "Content-Type: application/json" -X POST -d '{"name": "Top 10 ES6 Features", "url":"http://webapplog.com/es6", "text": "Nah not really"}'

echo -e "\nPUT /posts/0"
curl http://localhost:3000/posts/0 -H 'Content-Type: application/json' -X PUT -d '{"name": "Top 10 ES5 Features Every Developer Must Know", "url":"http://webapplog.com/es5", "text": "Sorry it is about ES5"}'

echo -e "\nDELETE /posts/1"
curl http://localhost:3000/posts/1 -X DELETE

echo -e "\nGET /posts"
curl http://localhost:3000/posts

echo -e "\nGET /posts/0/comments"
curl http://localhost:3000/posts/0/comments

echo -e "\nPOST /posts/0/comments"
curl http://localhost:3000/posts/0/comments -H "Content-Type: application/json" -X POST -d '{"text": "Really the top 10 features"}'

echo -e "\nPUT /posts/0/comments/3"
curl http://localhost:3000/posts/0/comments/3 -H "Content-Type: application/json" -X PUT -d '{"text": "NOT really the top 10 features"}'

echo -e "\nDELETE /posts/0/comments/3"
curl http://localhost:3000/posts/0/comments/3 -X DELETE

echo -e "\nGET /posts"
curl http://localhost:3000/posts